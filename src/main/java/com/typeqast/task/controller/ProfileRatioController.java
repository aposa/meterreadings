package com.typeqast.task.controller;

import com.typeqast.task.entity.ProfileRatio;
import com.typeqast.task.exceptions.EntityNotFoundException;
import com.typeqast.task.exceptions.InvalidEntitiesException;
import com.typeqast.task.service.ProfileRatioService;
import com.typeqast.task.validations.ValidatedInput;
import com.typeqast.task.validations.ValidatedProfiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.typeqast.task.validations.ProfileRatioValidations.validateProfiles;

@RestController
@RequestMapping("/ratios")
public class ProfileRatioController {

    @Autowired
    ProfileRatioService ratioService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<ProfileRatio> addRatios(@RequestBody List<ProfileRatio> ratios) throws InvalidEntitiesException {

        ValidatedProfiles validatedProfiles = validateProfiles(ratios);

        if(!validatedProfiles.getValid().isEmpty()){
            for (ProfileRatio ratio : ratios) {
                if(validatedProfiles.getValid().contains(ratio.getProfile())){
                    Long updateId = getIdIfExists(ratio.getProfile(), ratio.getMonth());
                    if(updateId > 0L) {
                        ratio.setId(updateId);
                    }
                    ratioService.save(ratio);
                }
            }
        } else {
            if(!validatedProfiles.getInvalid().isEmpty()){
                throw new InvalidEntitiesException(ProfileRatio.class, validatedProfiles.getInvalid());
            }
        }

        return new ResponseEntity(validatedProfiles, HttpStatus.CREATED);
    }

        @RequestMapping(method = RequestMethod.PUT)
        public ResponseEntity<ProfileRatio> updateRatios(@RequestBody List<ProfileRatio> ratios) throws InvalidEntitiesException {

            ValidatedInput validatedProfiles = validateProfiles(ratios);

            if(!validatedProfiles.getValid().isEmpty()){
                for (ProfileRatio ratio : ratios) {
                    if(validatedProfiles.getValid().contains(ratio.getProfile())){
                        ratioService.save(ratio);
                    }
                }
            } else {
                if(!validatedProfiles.getInvalid().isEmpty()){
                    throw new InvalidEntitiesException(ProfileRatio.class, validatedProfiles.getInvalid());
                }
            }
            return new ResponseEntity(validatedProfiles, HttpStatus.OK);
        }

       @RequestMapping(value = "/{id}", method = RequestMethod.GET)
        public ResponseEntity<ProfileRatio> getRatioById(@PathVariable("id") Long id) throws EntityNotFoundException {
        Optional<ProfileRatio> ratio = ratioService.getById(id);
        if (!ratio.isPresent()) {
            throw new EntityNotFoundException(ProfileRatio.class, "id", id.toString());
        }
           return new ResponseEntity<ProfileRatio>(ratio.get(), HttpStatus.OK);
        }

        @RequestMapping(params = "profile", method = RequestMethod.GET)
        public ResponseEntity<List<ProfileRatio>> getRatiosByProfile(@RequestParam(value="profile") String profile) throws EntityNotFoundException {
            Optional<List<ProfileRatio>> ratios = ratioService.getByProfile(profile);
            if (!ratios.isPresent()) {
                throw new EntityNotFoundException(ProfileRatio.class, "profile", profile);
            }
            return new ResponseEntity<List<ProfileRatio>>(ratios.get(), HttpStatus.OK);
        }

        @RequestMapping(params = "month", method = RequestMethod.GET)
        public ResponseEntity<List<ProfileRatio>> getRatiosByMonth(@RequestParam(value="month") String month) throws EntityNotFoundException {
            Optional<List<ProfileRatio>> ratios = ratioService.getByMonth(month);
            if (!ratios.isPresent()) {
                throw new EntityNotFoundException(ProfileRatio.class, "month", month);
            }
            return new ResponseEntity<List<ProfileRatio>>(ratios.get(), HttpStatus.OK);
        }

    @RequestMapping(params = { "profile", "month" }, method = RequestMethod.GET)
    public ResponseEntity<ProfileRatio> getRatioByProfileAndMonth(@RequestParam Map<String,String> requestParams) throws EntityNotFoundException {
        String profile=requestParams.get("profile");
        String month=requestParams.get("month");
        Optional<ProfileRatio> ratio = ratioService.getByProfileAndMonth(profile, month);
        if (!ratio.isPresent()) {
            throw new EntityNotFoundException(ProfileRatio.class, "profile", profile, "month", month);
        }
        return new ResponseEntity<ProfileRatio>(ratio.get(), HttpStatus.OK);
    }

        @RequestMapping(value = "/all", method = RequestMethod.GET)
        public ResponseEntity<List<ProfileRatio>> getAllRatios() {
            List<ProfileRatio> ratios = ratioService.getAll();
            if (ratios.isEmpty()) {
                return new ResponseEntity<List<ProfileRatio>>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<List<ProfileRatio>>(ratios, HttpStatus.OK);
        }


        @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
        public ResponseEntity<Void> deleteRatioById(@PathVariable("id") Long id) throws EntityNotFoundException {
            Optional<ProfileRatio> ratio = ratioService.getById(id);
            if (!ratio.isPresent()) {
                throw new EntityNotFoundException(ProfileRatio.class, "id", id.toString());
            } else {
                ratioService.delete(id);
                return new ResponseEntity<Void>(HttpStatus.GONE);
            }
        }

        @RequestMapping(method = RequestMethod.DELETE)
        public ResponseEntity<Void> deleteRatiosOfProfile(@RequestParam(value="profile") String profile) throws EntityNotFoundException {
            Optional<List<ProfileRatio>> ratios = ratioService.getByProfile(profile);
            if (!ratios.isPresent()) {
                throw new EntityNotFoundException(ProfileRatio.class, "profile", profile);
            } else {
                for (ProfileRatio ratio : ratios.get()) {
                    ratioService.delete(ratio.getId());
                }
                return new ResponseEntity<Void>(HttpStatus.GONE);
            }
        }

        @RequestMapping(value = "/all", method = RequestMethod.DELETE)
        public ResponseEntity<Void> deleteAllRatios() {
            List<ProfileRatio> ratios = ratioService.getAll();
            if (ratios.isEmpty()) {
                return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
            } else {
                for (ProfileRatio ratio : ratios) {
                    ratioService.delete(ratio.getId());
                }
                return new ResponseEntity<Void>(HttpStatus.GONE);
            }
        }

    public Long getIdIfExists(String profile, String month) {
        Long existingId = 0L;
        Optional<ProfileRatio> foundRatio = ratioService.getByProfileAndMonth(profile, month);
        if(foundRatio.isPresent()){
            existingId = foundRatio.get().getId();
        }
        return existingId;
    }

}

