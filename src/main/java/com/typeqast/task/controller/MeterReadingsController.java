package com.typeqast.task.controller;

import com.typeqast.task.entity.MeterReadings;
import com.typeqast.task.exceptions.EntityNotFoundException;
import com.typeqast.task.exceptions.InvalidEntitiesException;
import com.typeqast.task.service.MeterReadingsService;
import com.typeqast.task.validations.MeterReadingsValidations;
import com.typeqast.task.validations.ValidatedConnections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;


@RestController
@RequestMapping("/meters")
public class MeterReadingsController {

	@Autowired
    MeterReadingsService meterService;

    @Autowired
    MeterReadingsValidations validations;
	
	@RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<MeterReadings> addMeterReadings(@RequestBody List<MeterReadings> meters) throws InvalidEntitiesException {

	    ValidatedConnections validatedConnections = validations.validateConnections(meters);
        if(!validatedConnections.getValid().isEmpty()){
            for (MeterReadings meter : meters) {           	
                if(validatedConnections.getValid().contains(meter.getConnection())){
                    Long updateId = getIdIfExists(meter.getConnection(), meter.getMonth());
                    if(updateId > 0L) {
                    	continue;
                    } else {
                    	meterService.save(meter);
                    }
                    
                }
            }
        } else {
            if(!validatedConnections.getInvalid().isEmpty()){
                throw new InvalidEntitiesException(MeterReadings.class, validatedConnections.getInvalid());
            }
        }

        return new ResponseEntity(validatedConnections, HttpStatus.CREATED);
    }
	
	
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<MeterReadings> getMeterReadingById(@PathVariable("id") Long id) throws EntityNotFoundException {
    Optional<MeterReadings> meter = meterService.getById(id);
    if (!meter.isPresent()) {
        throw new EntityNotFoundException(MeterReadings.class, "id", id.toString());
    }
       return new ResponseEntity<MeterReadings>(meter.get(), HttpStatus.OK);
    }
    
    @RequestMapping(params = "connection", method = RequestMethod.GET)
    public ResponseEntity<List<MeterReadings>> getMeterReadingsByConnection(@RequestParam(value="connection") String connection) throws EntityNotFoundException {
        Optional<List<MeterReadings>> meters = meterService.getByConnection(connection);
        if (!meters.isPresent()) {
            throw new EntityNotFoundException(MeterReadings.class, "connection", connection.toString());
        }
        return new ResponseEntity<List<MeterReadings>>(meters.get(), HttpStatus.OK);
    }

    @RequestMapping(params = "profile", method = RequestMethod.GET)
    public ResponseEntity<List<MeterReadings>> getMeterReadingsByProfile(@RequestParam(value="profile") String profile) throws EntityNotFoundException {
        Optional<List<MeterReadings>> meters = meterService.getByProfile(profile);
        if (!meters.isPresent()) {
            throw new EntityNotFoundException(MeterReadings.class, "profile", profile);
        }
        return new ResponseEntity<List<MeterReadings>>(meters.get(), HttpStatus.OK);
    }

@RequestMapping(params = { "connection", "month" }, method = RequestMethod.GET)
public ResponseEntity<MeterReadings> getMeterReadingByConnectionAndMonth(@RequestParam Map<String,String> requestParams) throws EntityNotFoundException {
    String connection= requestParams.get("connection");
    String month=requestParams.get("month");
    Optional<MeterReadings> meter = meterService.getByConnectionAndMonth(connection, month);
    if (!meter.isPresent()) {
        throw new EntityNotFoundException(MeterReadings.class, "connection", connection, "month", month);
    }
    return new ResponseEntity<MeterReadings>(meter.get(), HttpStatus.OK);
}

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<MeterReadings>> getAllMeterReadings() {
        List<MeterReadings> meters = meterService.getAll();
        if (meters.isEmpty()) {
            return new ResponseEntity<List<MeterReadings>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<MeterReadings>>(meters, HttpStatus.OK);
    }
	
	 @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
     public ResponseEntity<Void> deleteMeterReadingById(@PathVariable("id") Long id) throws EntityNotFoundException {
         Optional<MeterReadings> meter = meterService.getById(id);
         if (!meter.isPresent()) {
             throw new EntityNotFoundException(MeterReadings.class, "id", id.toString());
         } else {
        	 meterService.delete(id);
             return new ResponseEntity<Void>(HttpStatus.GONE);
         }
     }

	@RequestMapping(params = "connection", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteMeterReadingsByConnection(@RequestParam(value="connection") String connection) throws EntityNotFoundException {
	    Optional<List<MeterReadings>> meters = meterService.getByConnection(connection);
        if (!meters.isPresent()) {
            throw new EntityNotFoundException(MeterReadings.class, "connection", connection);
        } else {
            for (MeterReadings meter : meters.get()) {
           	 meterService.delete(meter.getId());
            }
            return new ResponseEntity<Void>(HttpStatus.GONE);
        }
    }

     @RequestMapping(params = "profile",method = RequestMethod.DELETE)
     public ResponseEntity<Void> deleteMeterReadingsByProfile(@RequestParam(value="profile") String profile) throws EntityNotFoundException {
         Optional<List<MeterReadings>> meters = meterService.getByProfile(profile);
         if (!meters.isPresent()) {
             throw new EntityNotFoundException(MeterReadings.class, "profile", profile);
         } else {
             for (MeterReadings meter : meters.get()) {
            	 meterService.delete(meter.getId());
             }
             return new ResponseEntity<Void>(HttpStatus.GONE);
         }
     }

     @RequestMapping(value = "/all", method = RequestMethod.DELETE)
     public ResponseEntity<Void> deleteAllMeterReadings() {
         List<MeterReadings> meters = meterService.getAll();
         if (meters.isEmpty()) {
             return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
         } else {
             for (MeterReadings meter : meters) {
            	 meterService.delete(meter.getId());
             }
             return new ResponseEntity<Void>(HttpStatus.GONE);
         }
     }
     
     public Long getIdIfExists(String connection, String month) {
         Long existingId = 0L;
         Optional<MeterReadings> foundRatio = meterService.getByConnectionAndMonth(connection, month);
         if(foundRatio.isPresent()){
             existingId = foundRatio.get().getId();
         }
         return existingId;
     }
}
