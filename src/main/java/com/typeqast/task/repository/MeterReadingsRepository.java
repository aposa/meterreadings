package com.typeqast.task.repository;

import com.typeqast.task.entity.MeterReadings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface MeterReadingsRepository extends JpaRepository<MeterReadings, Long> {

	@Query(value = "SELECT * FROM METERS WHERE PROFILE = ?1",
            countQuery = "SELECT count(*) FROM METERS WHERE PROFILE = ?1",
            nativeQuery = true)
    Optional<List<MeterReadings>> findByProfile(String profile);

	@Query(value = "SELECT * FROM METERS WHERE CONNECTION = ?1",
            countQuery = "SELECT count(*) FROM METERS WHERE CONNECTION = ?1",
            nativeQuery = true)
    Optional<List<MeterReadings>> findByConnection(String connection);

	@Query(value = "SELECT * FROM METERS WHERE CONNECTION = ?1 AND MONTH = ?2",
            countQuery = "SELECT count(*) FROM METERS WHERE CONNECTION = ?1 AND MONTH = ?2",
            nativeQuery = true)
    Optional<MeterReadings> findByConnectionAndMonth(String connection, String month);

}
