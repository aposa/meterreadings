package com.typeqast.task.repository;

import com.typeqast.task.entity.ProfileRatio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface ProfileRatioRepository extends JpaRepository<ProfileRatio, Long> {

    @Query(value = "SELECT * FROM RATIOS WHERE PROFILE = ?1",
            countQuery = "SELECT count(*) FROM RATIOS WHERE PROFILE = ?1",
            nativeQuery = true)
    Optional<List<ProfileRatio>> findByProfile(String profile);

    @Query(value = "SELECT * FROM RATIOS WHERE MONTH = ?1",
            countQuery = "SELECT count(*) FROM RATIOS WHERE MONTH = ?1",
            nativeQuery = true)
    Optional<List<ProfileRatio>> findByMonth(String month);

    @Query(value = "SELECT * FROM RATIOS WHERE PROFILE = ?1 AND MONTH = ?2",
            countQuery = "SELECT count(*) FROM RATIOS WHERE PROFILE = ?1 AND MONTH = ?2",
            nativeQuery = true)
    Optional<ProfileRatio> findByProfileAndMonth(String profile, String month);

}
