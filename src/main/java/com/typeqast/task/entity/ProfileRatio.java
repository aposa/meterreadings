package com.typeqast.task.entity;

import javax.persistence.*;

@Entity
@Table(name = "ratios")
public class ProfileRatio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name="profile")
    private String profile;

    @Column(name="month")
    private String month;

    @Column(name="ratio")
    private Double ratio;

    public ProfileRatio(Long id, String profile, String month, Double ratio) {
        this.id = id;
        this.profile = profile;
        this.month = month;
        this.ratio = ratio;
    }

    public ProfileRatio(String profile, String month, Double ratio) {
        this.profile = profile;
        this.month = month;
        this.ratio = ratio;
    }

    public ProfileRatio() {

    }
    
    public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
    public String getProfile() {
		return profile;
	}
	
	public void setProfile(String profile) {
		this.profile = profile;
	}
	
    public String getMonth() {
		return month;
	}
	
	public void setMonth(String month) {
		this.month = month;
	}
	
    public Double getRatio() {
		return ratio;
	}
	
	public void setRatio(Double ratio) {
		this.ratio = ratio;
	}

}

