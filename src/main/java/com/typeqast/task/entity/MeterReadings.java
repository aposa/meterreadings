package com.typeqast.task.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "meters")
public class MeterReadings{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @NotNull
    @Column(name = "connection")
    private String connection;

    @Column(name="profile")
    private String profile;

    @Column(name="month")
    private String month;

    @Column(name="reading")
    private Long reading;
    
    public MeterReadings(Long id, String connection, String profile, String month, Long reading) {
        this.id = id;
        this.connection = connection;
        this.profile = profile;
        this.month = month;
        this.reading = reading;
    }

    public MeterReadings(String connection, String profile, String month, Long reading) {
    	this.connection = connection;
    	this.profile = profile;
        this.month = month;
        this.reading = reading;
    }

    public MeterReadings() {

    }
    
    public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getConnection() {
		return connection;
	}
	
	public void setConnection(String connection) {
		this.connection = connection;
	}
    
    public String getProfile() {
		return profile;
	}
	
	public void setProfile(String profile) {
		this.profile = profile;
	}
	
    public String getMonth() {
		return month;
	}
	
	public void setMonth(String month) {
		this.month = month;
	}
	
    public Long getReading() {
		return reading;
	}
	
	public void setReading(Long reading) {
		this.reading = reading;
	}

}
