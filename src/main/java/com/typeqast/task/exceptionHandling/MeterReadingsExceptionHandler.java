package com.typeqast.task.exceptionHandling;

import com.typeqast.task.exceptions.meters.InvalidConsumptionException;
import com.typeqast.task.exceptions.meters.InvalidMeterReadingException;
import com.typeqast.task.exceptions.meters.ProfileNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@Slf4j
public class MeterReadingsExceptionHandler extends RestExceptionHandler{

    @ExceptionHandler(InvalidConsumptionException.class)
    protected ResponseEntity<Object> handleInvalidConsumption(InvalidConsumptionException ex) {
        ApiError apiError = new ApiError(NOT_ACCEPTABLE);
        apiError.setMessage(ex.getMessage());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(InvalidMeterReadingException.class)
    protected ResponseEntity<Object> handleInvalidMeterReading(InvalidMeterReadingException ex) {
        ApiError apiError = new ApiError(NOT_ACCEPTABLE);
        apiError.setMessage(ex.getMessage());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(ProfileNotFoundException.class)
    protected ResponseEntity<Object> handleProfileNotFound(ProfileNotFoundException ex) {
        ApiError apiError = new ApiError(NOT_ACCEPTABLE);
        apiError.setMessage(ex.getMessage());
        return buildResponseEntity(apiError);
    }

}
