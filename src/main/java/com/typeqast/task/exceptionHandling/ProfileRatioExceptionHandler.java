package com.typeqast.task.exceptionHandling;

import com.typeqast.task.exceptions.profiles.EmptyEntityPropertyException;
import com.typeqast.task.exceptions.profiles.InvalidMonthException;
import com.typeqast.task.exceptions.profiles.InvalidRatioRangeException;
import com.typeqast.task.exceptions.profiles.InvalidSumOfRatiosException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@Slf4j
public class ProfileRatioExceptionHandler extends RestExceptionHandler {


    @ExceptionHandler(EmptyEntityPropertyException.class)
    protected ResponseEntity<Object> handleEmptyEntityProperty(EmptyEntityPropertyException ex) {
        ApiError apiError = new ApiError(NOT_ACCEPTABLE);
        apiError.setMessage(ex.getMessage());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(InvalidMonthException.class)
    protected ResponseEntity<Object> handleInvalidMonth(InvalidMonthException ex) {
        ApiError apiError = new ApiError(NOT_ACCEPTABLE);
        apiError.setMessage(ex.getMessage());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(InvalidRatioRangeException.class)
    protected ResponseEntity<Object> handleInvalidRatioRange(InvalidRatioRangeException ex) {
        ApiError apiError = new ApiError(NOT_ACCEPTABLE);
        apiError.setMessage(ex.getMessage());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(InvalidSumOfRatiosException.class)
    protected ResponseEntity<Object> handleInvalidSumOfRatios(InvalidSumOfRatiosException ex) {
        ApiError apiError = new ApiError(NOT_ACCEPTABLE);
        apiError.setMessage(ex.getMessage());
        return buildResponseEntity(apiError);
    }
}
