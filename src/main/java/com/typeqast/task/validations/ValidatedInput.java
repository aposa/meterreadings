package com.typeqast.task.validations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class ValidatedInput<E> {

	 	List<E> valid = new ArrayList<E>();
	    Map invalid = new HashMap();

	    public List<E> getValid() {
			return valid;
		}
		
		public void setValid(List<E> valid) {
			this.valid = valid;
		}
		
	    public Map getInvalid() {
			return invalid;
		}
		
		public void setInvalid(Map invalid) {
			this.invalid = invalid;
		}
}
