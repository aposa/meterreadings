package com.typeqast.task.validations;

import com.typeqast.task.entity.ProfileRatio;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormatSymbols;
import java.util.*;

public class ProfileRatioValidations {

    private static final DateFormatSymbols monthValues = new DateFormatSymbols(Locale.ENGLISH);

    public static ValidatedProfiles validateProfiles(List<ProfileRatio> ratios) {

       ValidatedProfiles validatedProfiles = checkProfilesForEmptyValues(ratios);
       if(!validatedProfiles.getValid().isEmpty()) {
           validatedProfiles = checkProfilesForInvalidMonthValues(ratios, validatedProfiles);

           if(!validatedProfiles.getValid().isEmpty()){
               validatedProfiles = checkProfilesForInvalidRatioRange(ratios, validatedProfiles);

               if(!validatedProfiles.getValid().isEmpty()){
                   validatedProfiles = checkProfilesForInvalidSums(ratios, validatedProfiles);
               }
           }
       }

       return validatedProfiles;
    }

    public static ValidatedProfiles checkProfilesForEmptyValues(List<ProfileRatio> ratios) {

        ValidatedProfiles validatedProfiles = new ValidatedProfiles();
        for (ProfileRatio ratio : ratios) {
            if(!validatedProfiles.getValid().contains(ratio.getProfile()) && (!ratio.getProfile().isEmpty() && !ratio.getMonth().isEmpty() && !ratio.getRatio().isInfinite())) {
                if(!validatedProfiles.getInvalid().containsKey(ratio.getProfile())) {
                    validatedProfiles.valid.add(ratio.getProfile());
                }
            } else if (!validatedProfiles.getInvalid().containsKey(ratio.getProfile()) && (ratio.getProfile().isEmpty() || ratio.getMonth().isEmpty() || ratio.getRatio().isInfinite())){
                validatedProfiles.invalid.put(ratio.getProfile(), " Empty values found in profile. ");
                if(validatedProfiles.getValid().contains(ratio.getProfile())) {
                    validatedProfiles.valid.remove(validatedProfiles.valid.indexOf(ratio.getProfile()));
                }
            }
        }

        return validatedProfiles;
    }

    public static ValidatedProfiles checkProfilesForInvalidMonthValues(List<ProfileRatio> ratios, ValidatedProfiles validatedProfiles) {

        String[] shortMonths = monthValues.getShortMonths();
        List<String> monthList = new ArrayList<String>();
        for(String shortMonth : shortMonths) {
            monthList.add(shortMonth.toUpperCase());
        }
        for (ProfileRatio ratio : ratios) {
            if (validatedProfiles.getValid().contains(ratio.getProfile())) {
                if(!(monthList.contains(ratio.getMonth()))){
                    validatedProfiles.valid.remove(validatedProfiles.valid.indexOf(ratio.getProfile()));
                    validatedProfiles.invalid.put(ratio.getProfile(), " Month values are not in expected format. ");
                }
            }
        }

       return validatedProfiles;
    }

    public static ValidatedProfiles checkProfilesForInvalidRatioRange(List<ProfileRatio> ratios, ValidatedProfiles validatedProfiles) {

        for (ProfileRatio ratio : ratios) {
            if (validatedProfiles.getValid().contains(ratio.getProfile())) {
                if(!(ratio.getRatio()>=0.0 && ratio.getRatio()<=1.0)){
                    validatedProfiles.valid.remove(validatedProfiles.valid.indexOf(ratio.getProfile()));
                    validatedProfiles.invalid.put(ratio.getProfile(), " Ratio values are not in allowed range. ");
                }
            }
        }

       return validatedProfiles;
    }

    public static ValidatedProfiles checkProfilesForInvalidSums(List<ProfileRatio> ratios, ValidatedProfiles validatedProfiles) {

       Map profileSum = new HashMap();
        if(!validatedProfiles.getValid().isEmpty()) {
            for (ProfileRatio ratio : ratios) {
                if (validatedProfiles.getValid().contains(ratio.getProfile())) {
                    if (profileSum.isEmpty()) {
                        profileSum.put(ratio.getProfile(), ratio.getRatio());
                    } else if (!profileSum.containsKey(ratio.getProfile())) {
                        profileSum.put(ratio.getProfile(), ratio.getRatio());
                    } else {
                        double current = (double) profileSum.get(ratio.getProfile());
                        profileSum.replace(ratio.getProfile(), (current + ratio.getRatio()));
                    }
                }
            }
        }
        for(Object key: profileSum.keySet()){
            if (new BigDecimal((Double)profileSum.get(key)).setScale(2, RoundingMode.HALF_UP).doubleValue() != 1.0) {
                if(validatedProfiles.getValid().contains(key)){
                    validatedProfiles.valid.remove(validatedProfiles.valid.indexOf(key));
                    validatedProfiles.invalid.put(key, " Sum of ratios is not equal to 1.0. ");
                }
            }
        }

        return validatedProfiles;
    }

}
