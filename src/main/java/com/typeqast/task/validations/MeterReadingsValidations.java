package com.typeqast.task.validations;

import com.typeqast.task.entity.MeterReadings;
import com.typeqast.task.entity.ProfileRatio;
import com.typeqast.task.service.ProfileRatioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Component
public class MeterReadingsValidations {

	@Autowired
	ProfileRatioService profileService;

    private static final DateFormatSymbols monthValues = new DateFormatSymbols(Locale.ENGLISH);
    private static final double TOLERANCE_PERCENTAGE = 0.25;

    public ValidatedConnections validateConnections(List<MeterReadings> meters) {

    	ValidatedConnections validatedConnections = checkForEmptyValues(meters);
       if(!validatedConnections.getValid().isEmpty()) {
    	   validatedConnections = checkForInvalidMonthValues(meters, validatedConnections);
           if(!validatedConnections.getValid().isEmpty()){
           	   validatedConnections = checkExistingProfiles(meters, validatedConnections);
               if(!validatedConnections.getValid().isEmpty()){
            	   validatedConnections = validateMeterReadingsPerConnection(meters, validatedConnections);
               }
           }
       }

       return validatedConnections;
    }

    public ValidatedConnections checkForEmptyValues(List<MeterReadings> meters) {

    	ValidatedConnections validatedConnections = new ValidatedConnections();
        for (MeterReadings meter : meters) {
            if(!validatedConnections.getValid().contains(meter.getConnection()) && (!meter.getProfile().isEmpty() && !meter.getMonth().isEmpty())) {
                if(!validatedConnections.getInvalid().containsKey(meter.getConnection())) {
                	validatedConnections.valid.add(meter.getConnection());
                }
            } else if (!validatedConnections.getInvalid().containsKey(meter.getConnection()) && (meter.getProfile().isEmpty() || meter.getMonth().isEmpty())){
            	validatedConnections.invalid.put(meter.getConnection(), " Empty values found in connection. ");
                if(validatedConnections.getValid().contains(meter.getConnection())) {
                	validatedConnections.valid.remove(validatedConnections.valid.indexOf(meter.getConnection()));
                }
            }
        }

        return validatedConnections;
    }

    public ValidatedConnections checkForInvalidMonthValues(List<MeterReadings> meters, ValidatedConnections validatedConnections) {

        String[] shortMonths = monthValues.getShortMonths();
        List<String> monthList = new ArrayList<String>();
        for(String shortMonth : shortMonths) {
            monthList.add(shortMonth.toUpperCase());
        }
        for (MeterReadings meter : meters) {
            if (validatedConnections.getValid().contains(meter.getConnection())) {
                if(!(monthList.contains(meter.getMonth()))){
                	validatedConnections.valid.remove(validatedConnections.valid.indexOf(meter.getConnection()));
                	validatedConnections.invalid.put(meter.getConnection(), " Connection month values are not in expected format. ");
                }
            }
        }

       return validatedConnections;
    }  

	public ValidatedConnections checkExistingProfiles(List<MeterReadings> meters, ValidatedConnections validatedConnections) {
    	for (MeterReadings meter : meters) {
	    	Optional<List<ProfileRatio>> ratios = profileService.getByProfile(meter.getProfile());
	        if (!ratios.isPresent()) {
	        	validatedConnections.invalid.put(meter.getProfile(), " Profile not found in database. ");
                if(validatedConnections.getValid().contains(meter.getConnection())) {
                	validatedConnections.valid.remove(validatedConnections.valid.indexOf(meter.getConnection()));
                }
	        }
    	}  	
    	return validatedConnections;  	
    }  
         
    public ValidatedConnections validateMeterReadingsPerConnection(List<MeterReadings> meters, ValidatedConnections validatedConnections) {
 		   	 
         if(!validatedConnections.getValid().isEmpty()) {  
        	 for (String validConnection : validatedConnections.getValid()) {
        		 List<MeterReadings> singleConnectionMeterReadings = new ArrayList<MeterReadings>();
        		 for (MeterReadings meter : meters) {   
        			 if (validConnection.equals(meter.getConnection())) {
        				singleConnectionMeterReadings.add(meter);
        			 }
        		 } 

				 singleConnectionMeterReadings = sortConnectionByMonths(singleConnectionMeterReadings);

        		 validatedConnections = checkMonthsMeterReading(validConnection, singleConnectionMeterReadings, validatedConnections);
        		 if(validatedConnections.getInvalid().containsKey(validConnection)){
        			break;
        		 }
        		 
        		validatedConnections = checkMonthsConsumption(validConnection, singleConnectionMeterReadings, validatedConnections);
        		 if(validatedConnections.getInvalid().containsKey(validConnection)){
        			break;
        		 }
        		 
        	 }    
         }

		List<String> connectionsToRemoveFromValid = new ArrayList<String>();
         for (String validConnection : validatedConnections.getValid()) {
	         if(validatedConnections.getInvalid().containsKey(validConnection)) {
				 connectionsToRemoveFromValid.add(validConnection);
	         }
         }

         if(!connectionsToRemoveFromValid.isEmpty()){
         	for(String connToRemove : connectionsToRemoveFromValid){
				validatedConnections.valid.remove(validatedConnections.valid.indexOf(connToRemove));
			}
		 }
         
         return validatedConnections;  
    }
    	
    public ValidatedConnections checkMonthsMeterReading(String validConnection, List<MeterReadings> connMeterReadings, ValidatedConnections validatedConnections) {
    	Long prev = 0L, current;
    	for (MeterReadings meter : connMeterReadings) {
    		current = meter.getReading();
    		if(current < prev){   			   			             
    			validatedConnections.invalid.put(validConnection, " Connection meter readings are not increasing by month. ");
    			break;
    		} 
    		prev = current;
    	}
    	return validatedConnections;  	
    }
    
    public ValidatedConnections checkMonthsConsumption(String validConnection, List<MeterReadings> connMeterReadings, ValidatedConnections validatedConnections) {

    	long prev = 0L, current;
    	long yearsConsumption = getYearsConsumption(connMeterReadings);

		for (MeterReadings meter : connMeterReadings) {
    		current = meter.getReading();

	    	double monthsConsumption = current - prev;
    		double monthsRatio = getMonthsRatio(meter.getProfile(), meter.getMonth());
    		double expectedConsumption = monthsRatio * yearsConsumption;
    		double toleranceValue = TOLERANCE_PERCENTAGE * expectedConsumption;
	    	double minValue = expectedConsumption - toleranceValue;
	    	double maxValue = expectedConsumption + toleranceValue;
	    	
	    	if(monthsConsumption >= minValue && monthsConsumption <= maxValue){
	    		prev = current;
	    	} else {
    			validatedConnections.invalid.put(validConnection, " Connection month consumptions are not inside tolerance percentage. ");
    			break;
	    	}

    	}  	
    	return validatedConnections;  	
 	
    }	
    
   private List<MeterReadings> sortConnectionByMonths(List<MeterReadings> meters) {

	   String[] shortMonths = new DateFormatSymbols(Locale.ENGLISH).getShortMonths();
	   List<MeterReadings> sortedConnection = new ArrayList<MeterReadings>();

	   for (int i = 0; i < 12; i++) {
		   shortMonths[i] = shortMonths[i].toUpperCase();

		   for(MeterReadings meter : meters){
				if(meter.getMonth().equals(shortMonths[i])){
					sortedConnection.add(meter);
					break;
				}
		   }
	   }
    	return sortedConnection;
    }
    
    private double getMonthsRatio(String profile, String month){
    	
    	double monthsRatio = 0.0;
    	Optional<ProfileRatio> ratio = profileService.getByProfileAndMonth(profile, month);
    	if (ratio.isPresent()) {
    		monthsRatio = ratio.get().getRatio();
        }
    	return monthsRatio;
    }
	
    private long getYearsConsumption(List<MeterReadings> meters){
    	long totalConsumption = 0L;
		long prev = 0L, current;

    	for (MeterReadings meter : meters) {
			current = meter.getReading();
    		totalConsumption += (current-prev);
			prev = current;
    	}

    	return totalConsumption;
    }
}
