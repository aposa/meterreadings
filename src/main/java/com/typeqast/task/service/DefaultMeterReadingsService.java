package com.typeqast.task.service;

import com.typeqast.task.entity.MeterReadings;
import com.typeqast.task.repository.MeterReadingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DefaultMeterReadingsService implements MeterReadingsService{

    @Autowired
    private MeterReadingsRepository meterRepository;

    @Override
    public MeterReadings save(MeterReadings entity) {
        return meterRepository.save(entity);
    }

    @Override
    public Optional<MeterReadings> getById(Long id) {
        return meterRepository.findById(id);
    }

    @Override
    public List<MeterReadings> getAll() {
        return meterRepository.findAll();
    }

    @Override
    public void delete(Long id) {
    	meterRepository.delete(getById(id).get());
    }
    
    @Override
    public Optional<List<MeterReadings>> getByProfile(String profile) {
        return meterRepository.findByProfile(profile);
    }

    @Override
    public Optional<List<MeterReadings>> getByConnection(String connection) {
    	return meterRepository.findByConnection(connection);
    }

    @Override
    public Optional<MeterReadings> getByConnectionAndMonth(String connection, String month) {
    	return meterRepository.findByConnectionAndMonth(connection, month);
    }
}
