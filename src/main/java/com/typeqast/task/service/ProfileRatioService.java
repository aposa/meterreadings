package com.typeqast.task.service;

import com.typeqast.task.entity.ProfileRatio;

import java.util.List;
import java.util.Optional;

public interface ProfileRatioService extends CRUDService<ProfileRatio>{

    Optional<List<ProfileRatio>> getByProfile(String profile);

    Optional<List<ProfileRatio>> getByMonth(String month);

    Optional<ProfileRatio> getByProfileAndMonth(String profile, String month);

}

