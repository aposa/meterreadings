package com.typeqast.task.service;

import com.typeqast.task.entity.MeterReadings;

import java.util.List;
import java.util.Optional;

public interface MeterReadingsService extends CRUDService<MeterReadings>{

    Optional<List<MeterReadings>> getByProfile(String profile);

    Optional<List<MeterReadings>> getByConnection(String connection);

    Optional<MeterReadings> getByConnectionAndMonth(String connection, String month);
    
}
