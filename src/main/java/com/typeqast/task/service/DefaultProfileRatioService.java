package com.typeqast.task.service;

import com.typeqast.task.entity.ProfileRatio;
import com.typeqast.task.repository.ProfileRatioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DefaultProfileRatioService implements ProfileRatioService {

    @Autowired
    private ProfileRatioRepository ratioRepository;

    @Override
    public ProfileRatio save(ProfileRatio entity){
        return ratioRepository.save(entity);
    }

    @Override
    public Optional<ProfileRatio> getById(Long id) {
       return ratioRepository.findById(id);
    }

    @Override
    public List<ProfileRatio> getAll() {
        return ratioRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        ratioRepository.delete(getById(id).get());
    }

    @Override
    public Optional<List<ProfileRatio>> getByProfile(String profile) {
        return ratioRepository.findByProfile(profile);
    }

    @Override
    public Optional<List<ProfileRatio>> getByMonth(String month) {
        return ratioRepository.findByMonth(month);
    }

    @Override
    public Optional<ProfileRatio> getByProfileAndMonth(String profile, String month) {
            return ratioRepository.findByProfileAndMonth(profile, month);
    }
}