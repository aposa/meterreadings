package com.typeqast.task.integrationTests;

import com.typeqast.task.TestHelper;
import com.typeqast.task.entity.MeterReadings;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:beforeTestRatios.sql")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:beforeTestMeters.sql")

@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:afterTestRatios.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:afterTestMeters.sql")

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MeterReadingsControllerIT {

	  @Autowired
	    MockMvc mockMvc;
	  
	    @Test
	    public void addMeterReadings_Success() throws Exception {

	        Long[] readings = {5L, 12L, 21L, 33L, 47L, 66L, 88L, 112L, 138L, 169L, 203L, 239L};
	        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
	        List<MeterReadings> meterReadings = TestHelper.generateMeterReadings("0005", "test", includedMonths, readings);

	        mockMvc.perform(post("/meters")
	                .contentType(TestHelper.APPLICATION_JSON_UTF8)
	                .content(TestHelper.convertListToJsonBytes(meterReadings))
	        )
	                .andExpect(status().isCreated())
	                .andExpect(content().contentType(TestHelper.APPLICATION_JSON_UTF8))
	                .andExpect(jsonPath("$.valid[0]", is("0005")))
	                .andExpect( jsonPath("$.invalid").isEmpty());
	    }

	    @Test
	    public void addMeterReadings_InvalidEntities() throws Exception {

	    	Long[] readings = {1L, 2L, 3L, 4L, 5L, 3L, 7L, 8L, 9L, 10L, 11L, 12L};
	        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
	        List<MeterReadings> meterReadings = TestHelper.generateMeterReadings("0005", "new", includedMonths, readings);

	        mockMvc.perform(post("/meters")
	                .contentType(TestHelper.APPLICATION_JSON_UTF8)
	                .content(TestHelper.convertListToJsonBytes(meterReadings))
	        )
	                .andExpect(status().isNotAcceptable());
	    }
	    
	    @Test
	    public void getMeterReadingById_Success() throws Exception {
	        mockMvc.perform(get("/meters/10"))
	                .andExpect(status().isOk())
	                .andExpect(content().contentType(TestHelper.APPLICATION_JSON_UTF8))
	                .andExpect( jsonPath("connection", is("0001")))
	                .andExpect( jsonPath("profile", is("new")))
	                .andExpect( jsonPath("month", is("OCT")))
	                .andExpect( jsonPath("reading", is(18)));
	    }

	    @Test
	    public void getMeterReadingById_NotFound() throws Exception {

	        mockMvc.perform(get("/meters/77")
	                .contentType(TestHelper.APPLICATION_JSON_UTF8)
	        )
	                .andExpect(status().isNotFound());
	    } 
	    
	    @Test
	    public void getMeterReadingsByConnection_Success() throws Exception {
	        mockMvc.perform(get("/meters?connection=0001"))
	                .andExpect(status().isOk())
	                .andExpect(content().contentType(TestHelper.APPLICATION_JSON_UTF8))
	                .andExpect(jsonPath("$", hasSize(12)))
	                .andExpect( jsonPath("$[10].connection", is("0001")))
	                .andExpect( jsonPath("$[10].profile", is("new")))
	                .andExpect( jsonPath("$[10].month", is("NOV")))
	                .andExpect( jsonPath("$[10].reading", is(19)));
	    }

	    @Test
	    public void getMeterReadingsByConnection_NotFound() throws Exception {

	        mockMvc.perform(get("/meters?connection=1234")
	                .contentType(TestHelper.APPLICATION_JSON_UTF8)
	        )
	                .andExpect(status().isNotFound());
	    }
	    
	    @Test
	    public void getMeterReadingsByProfile_Success() throws Exception {
	        mockMvc.perform(get("/meters?profile=new"))
	                .andExpect(status().isOk())
	                .andExpect(content().contentType(TestHelper.APPLICATION_JSON_UTF8))
	                .andExpect(jsonPath("$", hasSize(12)))
	                .andExpect( jsonPath("$[10].connection", is("0001")))
	                .andExpect( jsonPath("$[10].profile", is("new")))
	                .andExpect( jsonPath("$[10].month", is("NOV")))
	                .andExpect( jsonPath("$[10].reading", is(19)));
	    }

	    @Test
	    public void getMeterReadingsByProfile_NotFound() throws Exception {

	        mockMvc.perform(get("/meters?profile=abc")
	                .contentType(TestHelper.APPLICATION_JSON_UTF8)
	        )
	                .andExpect(status().isNotFound());
	    }
	    
	    @Test
	    public void getMeterReadingByConnectionAndMonth_Success() throws Exception {
	        mockMvc.perform(get("/meters?connection=0001&month=OCT"))
	                .andExpect(status().isOk())
	                .andExpect(content().contentType(TestHelper.APPLICATION_JSON_UTF8))
	                .andExpect( jsonPath("connection", is("0001")))
	                .andExpect( jsonPath("profile", is("new")))
	                .andExpect( jsonPath("month", is("OCT")))
	                .andExpect( jsonPath("reading", is(18)));
	    }

	    @Test
	    public void getMeterReadingByConnectionAndMonth_NotFound() throws Exception {

	        mockMvc.perform(get("/meters?connection=0001&month=ABC")
	                .contentType(TestHelper.APPLICATION_JSON_UTF8)
	        )
	                .andExpect(status().isNotFound());
	    }

	    @Test
	    public void getAllMeterReadings() throws Exception {
	        mockMvc.perform(get("/meters/all"))
	                .andExpect(status().isOk())
	                .andExpect(content().contentType(TestHelper.APPLICATION_JSON_UTF8))
	                .andExpect(jsonPath("$", hasSize(24)))
	                .andExpect( jsonPath("$[0].connection", is("0001")))
	                .andExpect( jsonPath("$[0].profile", is("new")))
	                .andExpect( jsonPath("$[0].month", is("JAN")))
	                .andExpect( jsonPath("$[0].reading", is(5)))
	                .andExpect( jsonPath("$[12].connection", is("0002")))
	                .andExpect( jsonPath("$[12].profile", is("old")))
	                .andExpect( jsonPath("$[12].month", is("JAN")))
	                .andExpect( jsonPath("$[12].reading", is(5)));
	    }
	    
	    @Test
	    public void deleteMeterReadingById_Success() throws Exception {

	        mockMvc.perform(delete("/meters/7")
	                .contentType(TestHelper.APPLICATION_JSON_UTF8)
	        )
	                .andExpect(status().isGone());
	    }

	    @Test
	    public void deleteMeterReadingById_NotFound() throws Exception {

	        mockMvc.perform(delete("/meters/77")
	                .contentType(TestHelper.APPLICATION_JSON_UTF8)
	        )
	                .andExpect(status().isNotFound());
	    }
	    
	    @Test
	    public void deleteMeterReadingsByConnection_Success() throws Exception {

	        mockMvc.perform(delete("/meters?connection=0002")
	                .contentType(TestHelper.APPLICATION_JSON_UTF8)
	        )
	                .andExpect(status().isGone());
	    }

	    @Test
	    public void deleteMeterReadingsByConnection_NotFound() throws Exception {

	        mockMvc.perform(delete("/meters?connection=1234")
	                .contentType(TestHelper.APPLICATION_JSON_UTF8)
	        )
	                .andExpect(status().isNotFound());
	    }

	    @Test
	    public void deleteMeterReadingsByProfile_Success() throws Exception {

	        mockMvc.perform(delete("/meters?profile=new")
	                .contentType(TestHelper.APPLICATION_JSON_UTF8)
	        )
	                .andExpect(status().isGone());
	    }

	    @Test
	    public void deleteMeterReadingsByProfile_NotFound() throws Exception {

	        mockMvc.perform(delete("/meters?profile=none")
	                .contentType(TestHelper.APPLICATION_JSON_UTF8)
	        )
	                .andExpect(status().isNotFound());
	    }

	    @Test
	    public void deleteAllMeterReadings() throws Exception {

	        mockMvc.perform(delete("/meters/all")
	                .contentType(TestHelper.APPLICATION_JSON_UTF8)
	        )
	                .andExpect(status().isGone());
	    }
}
