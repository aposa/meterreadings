package com.typeqast.task.integrationTests;

import com.typeqast.task.TestHelper;
import com.typeqast.task.entity.ProfileRatio;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:beforeTestRatios.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:afterTestRatios.sql")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProfileRatioControllerIT {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void addRatios_Success() throws Exception {

        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<ProfileRatio> profileRatios = TestHelper.generateProfileRatios("added", includedMonths, ratios);

        mockMvc.perform(post("/ratios")
                .contentType(TestHelper.APPLICATION_JSON_UTF8)
                .content(TestHelper.convertListToJsonBytes(profileRatios))
        )
                .andExpect(status().isCreated())
                .andExpect(content().contentType(TestHelper.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.valid[0]", is("added")))
                .andExpect( jsonPath("$.invalid").isEmpty());
    }

    @Test
    public void addRatios_InvalidEntities() throws Exception {

        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.8, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<ProfileRatio> profileRatios = TestHelper.generateProfileRatios("added", includedMonths, ratios);

        mockMvc.perform(post("/ratios")
                .contentType(TestHelper.APPLICATION_JSON_UTF8)
                .content(TestHelper.convertListToJsonBytes(profileRatios))
        )
                .andExpect(status().isNotAcceptable());
    }


    @Test
    public void updateRatios_Success() throws Exception {

        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<ProfileRatio> profileRatios = TestHelper.generateProfileRatios("test", includedMonths, ratios);

        mockMvc.perform(put("/ratios")
                .contentType(TestHelper.APPLICATION_JSON_UTF8)
                .content(TestHelper.convertListToJsonBytes(profileRatios))
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestHelper.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.valid[0]", is("test")))
                .andExpect( jsonPath("$.invalid").isEmpty());
    }

    @Test
    public void updateRatios_InvalidEntities() throws Exception {

        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.8, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<ProfileRatio> profileRatios = TestHelper.generateProfileRatios("test", includedMonths, ratios);

        mockMvc.perform(put("/ratios")
                .contentType(TestHelper.APPLICATION_JSON_UTF8)
                .content(TestHelper.convertListToJsonBytes(profileRatios))
        )
                .andExpect(status().isNotAcceptable());
    }


    @Test
    public void getRatioById_Success() throws Exception {
        mockMvc.perform(get("/ratios/10"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestHelper.APPLICATION_JSON_UTF8))
                .andExpect( jsonPath("profile", is("test")))
                .andExpect( jsonPath("month", is("OCT")))
                .andExpect( jsonPath("ratio", is(0.13)));
    }

    @Test
    public void getRatioById_NotFound() throws Exception {

        mockMvc.perform(get("/ratios/77")
                .contentType(TestHelper.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isNotFound());
    }

    @Test
    public void getRatiosByProfile_Success() throws Exception {
        mockMvc.perform(get("/ratios?profile=test"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestHelper.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(12)))
                .andExpect( jsonPath("$[2].profile", is("test")))
                .andExpect( jsonPath("$[2].month", is("MAR")))
                .andExpect( jsonPath("$[2].ratio", is(0.04)));
    }

    @Test
    public void getRatiosByProfile_NotFound() throws Exception {

        mockMvc.perform(get("/ratios?profile=abc")
                .contentType(TestHelper.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isNotFound());
    }

    @Test
    public void getRatiosByMonth_Success() throws Exception {
        mockMvc.perform(get("/ratios?month=AUG"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestHelper.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect( jsonPath("$[0].profile", is("test")))
                .andExpect( jsonPath("$[0].month", is("AUG")))
                .andExpect( jsonPath("$[0].ratio", is(0.10)))
                .andExpect( jsonPath("$[1].profile", is("fail")))
                .andExpect( jsonPath("$[1].month", is("AUG")))
                .andExpect( jsonPath("$[1].ratio", is(0.03)));

    }

    @Test
    public void getRatiosByMonth_NotFound() throws Exception {

        mockMvc.perform(get("/ratios?month=ABC")
                .contentType(TestHelper.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isNotFound());
    }

    @Test
    public void getRatioByProfileAndMonth_Success() throws Exception {
        mockMvc.perform(get("/ratios?profile=test&month=JUN"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestHelper.APPLICATION_JSON_UTF8))
                .andExpect( jsonPath("profile", is("test")))
                .andExpect( jsonPath("month", is("JUN")))
                .andExpect( jsonPath("ratio", is(0.08)));
    }

    @Test
    public void getRatiosByProfileAndMonth_NotFound() throws Exception {

        mockMvc.perform(get("/ratios?profile=test&month=ABC")
                .contentType(TestHelper.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isNotFound());
    }

    @Test
    public void getAllRatios() throws Exception {
        mockMvc.perform(get("/ratios/all"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestHelper.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(24)))
                .andExpect( jsonPath("$[0].profile", is("test")))
                .andExpect( jsonPath("$[0].month", is("JAN")))
                .andExpect( jsonPath("$[0].ratio", is(0.02)))
                .andExpect( jsonPath("$[1].profile", is("test")))
                .andExpect( jsonPath("$[1].month", is("FEB")))
                .andExpect( jsonPath("$[1].ratio", is(0.03)));
    }


    @Test
    public void deleteRatioById_Success() throws Exception {

        mockMvc.perform(delete("/ratios/7")
                .contentType(TestHelper.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isGone());
    }

    @Test
    public void deleteRatioById_NotFound() throws Exception {

        mockMvc.perform(delete("/ratios/77")
                .contentType(TestHelper.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteRatiosOfProfile_Success() throws Exception {

        mockMvc.perform(delete("/ratios?profile=test")
                .contentType(TestHelper.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isGone());
    }

    @Test
    public void deleteRatiosOfProfile_NotFound() throws Exception {

        mockMvc.perform(delete("/ratios?profile=none")
                .contentType(TestHelper.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteAllRatios() throws Exception {

        mockMvc.perform(delete("/ratios/all")
                .contentType(TestHelper.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isGone());
    }

}