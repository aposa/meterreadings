package com.typeqast.task.unitTests;

import com.typeqast.task.entity.ProfileRatio;
import org.junit.Test;
import org.modelmapper.ModelMapper;

import static org.junit.Assert.assertEquals;

public class ProfileRatioUT {
    private static final ModelMapper modelMapper = new ModelMapper();

    @Test
    public void checkProfileRatioMapping() {

        ProfileRatio testProfile = new ProfileRatio("testProfile", "JUN", 0.13);

        ProfileRatio ratioMapper = modelMapper.map(testProfile, ProfileRatio.class);
        assertEquals(testProfile.getProfile(), ratioMapper.getProfile());
        assertEquals(testProfile.getMonth(), ratioMapper.getMonth());
        assertEquals(testProfile.getRatio(), ratioMapper.getRatio());

    }

}