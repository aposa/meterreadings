package com.typeqast.task.unitTests;

import com.typeqast.task.entity.MeterReadings;
import org.junit.Test;
import org.modelmapper.ModelMapper;

import static org.junit.Assert.assertEquals;

public class MeterReadingsUT {
	private static final ModelMapper modelMapper = new ModelMapper();

    @Test
    public void checkMeterReadingsMapping() {

    	MeterReadings testMeterReadings = new MeterReadings("1111", "test", "JUN", 13L);

    	MeterReadings meterMapper = modelMapper.map(testMeterReadings, MeterReadings.class);
    	assertEquals(testMeterReadings.getConnection(), meterMapper.getConnection());
    	assertEquals(testMeterReadings.getProfile(), meterMapper.getProfile());
        assertEquals(testMeterReadings.getMonth(), meterMapper.getMonth());
        assertEquals(testMeterReadings.getReading(), meterMapper.getReading());

    }

}
