package com.typeqast.task.unitTests;

import com.typeqast.task.TestHelper;
import com.typeqast.task.entity.ProfileRatio;
import com.typeqast.task.service.ProfileRatioService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProfileRatioControllerUT {

    @Autowired
    private MockMvc mockMvc;
    
    @MockBean
    ProfileRatioService ratioServiceMock;

    @Test
    public void addRatios() throws Exception {

        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<ProfileRatio> testProfiles = TestHelper.generateProfileRatios("added", includedMonths, ratios);

        for (ProfileRatio ratio : testProfiles) {
            when(ratioServiceMock.save(ratio)).thenReturn(ratio);
        }

        mockMvc.perform(post("/ratios")
                .contentType(TestHelper.APPLICATION_JSON_UTF8)
                .content(TestHelper.convertListToJsonBytes(testProfiles))
        )
                .andExpect(status().isCreated())
                .andExpect(content().contentType(TestHelper.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.valid[0]", is("added")))
                .andExpect( jsonPath("$.invalid").isEmpty());
    }

    @Test
    public void getAllRatios() throws Exception {

    	List<ProfileRatio> testProfiles = new ArrayList<>();
    	testProfiles.add(new ProfileRatio("test", "JUN", 0.13));
        testProfiles.add(new ProfileRatio("test", "JUL", 0.15));

        when(ratioServiceMock.getAll()).thenReturn(testProfiles);

        mockMvc.perform(get("/ratios/all"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestHelper.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void deleteAllProfiles() throws Exception {

        List<ProfileRatio> testProfiles = new ArrayList<>();
        testProfiles.add(new ProfileRatio("test", "JUN", 0.13));
        testProfiles.add(new ProfileRatio("test", "JUL", 0.15));

        when(ratioServiceMock.getAll()).thenReturn(testProfiles);

        for (ProfileRatio ratio : testProfiles) {
            ratioServiceMock.delete(ratio.getId());
        }

        mockMvc.perform(delete("/ratios/all")
                .contentType(TestHelper.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isGone());
    }

}
