package com.typeqast.task.unitTests;

import com.typeqast.task.TestHelper;
import com.typeqast.task.entity.MeterReadings;
import com.typeqast.task.service.MeterReadingsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MeterReadingsControllerUT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    MeterReadingsService meterServiceMock;

    @Test
    public void getAllMeterReadings() throws Exception {

        List<MeterReadings> testReadings = new ArrayList<>();
        testReadings.add(new MeterReadings("0005","test", "JUN", 100L));
        testReadings.add(new MeterReadings("0005","test", "JUL", 120L));


        when(meterServiceMock.getAll()).thenReturn(testReadings);

        mockMvc.perform(get("/meters/all"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestHelper.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void deleteAllMeterReadings() throws Exception {

        List<MeterReadings> testReadings = new ArrayList<>();
        testReadings.add(new MeterReadings("0005","test", "JUN", 100L));
        testReadings.add(new MeterReadings("0005","test", "JUL", 120L));

        when(meterServiceMock.getAll()).thenReturn(testReadings);

        for (MeterReadings meter : testReadings) {
            meterServiceMock.delete(meter.getId());
        }

        mockMvc.perform(delete("/meters/all")
                .contentType(TestHelper.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isGone());
    }

    @Test
    public void addMeterReadings_noProfileFound() throws Exception {

        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        Long[] readings = {5L, 12L, 21L, 33L, 47L, 66L, 88L, 112L, 138L, 169L, 203L, 239L};
        List<MeterReadings> testReadings = TestHelper.generateMeterReadings("0005", "test", includedMonths, readings);

        for (MeterReadings meter : testReadings) {
            when(meterServiceMock.save(meter)).thenReturn(meter);
        }

        mockMvc.perform(post("/meters")
                .contentType(TestHelper.APPLICATION_JSON_UTF8)
                .content(TestHelper.convertListToJsonBytes(testReadings))
        )
                .andExpect(status().isNotAcceptable());
    }

}
