package com.typeqast.task.unitTests;

import com.typeqast.task.TestHelper;
import com.typeqast.task.entity.MeterReadings;
import com.typeqast.task.validations.MeterReadingsValidations;
import com.typeqast.task.validations.ValidatedInput;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MeterReadingsValidationsUT {

    @Autowired
    private MeterReadingsValidations validations;

   @Test
    public void checkProfilesForEmptyValues() {

       int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
       Long[] readings = {5L, 12L, 21L, 33L, 47L, 66L, 88L, 112L, 138L, 169L, 203L, 239L};
       List<MeterReadings> meterReadings = TestHelper.generateMeterReadings("0005", "test", includedMonths, readings);

        ValidatedInput validated = validations.checkForEmptyValues(meterReadings);

        Assert.assertEquals("Valid: ", 1, validated.getValid().size());
        Assert.assertEquals("Invalid: ", 0, validated.getInvalid().size());
    }


    @Test
    public void validateConnections_noProfileFound() {

        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        Long[] readings = {5L, 12L, 21L, 33L, 47L, 66L, 88L, 112L, 138L, 169L, 203L, 239L};
        List<MeterReadings> meterReadings = TestHelper.generateMeterReadings("0005", "test", includedMonths, readings);
        Long[] readings_fail = {1L, 2L, 3L, 4L, 5L, 3L, 7L, 8L, 9L, 10L, 11L, 12L};
        List<MeterReadings> meterReadings_fail = TestHelper.generateMeterReadings("0005", "new", includedMonths, readings_fail);
        meterReadings.addAll(meterReadings_fail);

        ValidatedInput validated = validations.validateConnections(meterReadings);

        Assert.assertEquals("Valid: ", 0, validated.getValid().size());
        Assert.assertEquals("Invalid: ", 2, validated.getInvalid().size());
    }

}
