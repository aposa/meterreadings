package com.typeqast.task.unitTests;

import com.typeqast.task.TestHelper;
import com.typeqast.task.entity.ProfileRatio;
import com.typeqast.task.validations.ProfileRatioValidations;
import com.typeqast.task.validations.ValidatedInput;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProfileRatioValidationsUT {

    @Test
    public void checkProfilesForEmptyValues() {

        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.8, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<ProfileRatio> profileRatios = TestHelper.generateProfileRatios("test", includedMonths, ratios);
        List<ProfileRatio> updateRatios = TestHelper.generateProfileRatios("update", includedMonths, ratios);
        profileRatios.addAll(updateRatios);
        List<ProfileRatio> invalidRatios = TestHelper.generateProfileRatios("", includedMonths, ratios);
        profileRatios.addAll(invalidRatios);

        ValidatedInput validated = ProfileRatioValidations.checkProfilesForEmptyValues(profileRatios);

        Assert.assertEquals("Valid profiles: ", 2, validated.getValid().size());
        Assert.assertEquals("Invalid profiles: ", 1, validated.getInvalid().size());
    }


    @Test
    public void validateProfiles() {

        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1};
        Double[] invalidRatios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 1.8, 0.1, 0.1, 0.05, 0.05, 0.1};
        Double[] invalidSumRatios = {0.1, 0.1, 0.8, 0.1, 0.05, 0.05, 0.8, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<ProfileRatio> profileRatios = TestHelper.generateProfileRatios("test", includedMonths, ratios);
        List<ProfileRatio> invalidProfileRatios = TestHelper.generateProfileRatios("ratioFail", includedMonths, invalidRatios);
        profileRatios.addAll(invalidProfileRatios);
        List<ProfileRatio> updateRatios = TestHelper.generateProfileRatios("update", includedMonths, ratios);
        profileRatios.addAll(updateRatios);
        List<ProfileRatio> invalidProfileSumRatios = TestHelper.generateProfileRatios("sumFail", includedMonths, invalidSumRatios);
        profileRatios.addAll(invalidProfileSumRatios);

        ValidatedInput validated = ProfileRatioValidations.validateProfiles(profileRatios);

        Assert.assertEquals("Valid profiles: ", 2, validated.getValid().size());
        Assert.assertEquals("Invalid profiles: ", 2, validated.getInvalid().size());
    }



}
