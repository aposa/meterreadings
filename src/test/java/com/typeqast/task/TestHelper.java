package com.typeqast.task;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.typeqast.task.entity.MeterReadings;
import com.typeqast.task.entity.ProfileRatio;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class TestHelper {

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    public static <T> byte[] convertListToJsonBytes(List<T> list) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(list);
    }

    public static List<ProfileRatio> generateProfileRatios(String profile, int[] includedMonths, Double[] ratios) {
        String[] shortMonths = new DateFormatSymbols(Locale.ENGLISH).getShortMonths();
        List<ProfileRatio> profileRatios = new ArrayList<>();
        for (int i = 0; i < includedMonths.length; i++) {
            profileRatios.add(new ProfileRatio(profile, shortMonths[includedMonths[i]].toUpperCase(), ratios[i]));
        }

        return profileRatios;
    }
    
    public static List<MeterReadings> generateMeterReadings(String connection, String profile, int[] includedMonths, Long[] meter_readings) {
        String[] shortMonths = new DateFormatSymbols(Locale.ENGLISH).getShortMonths();
        List<MeterReadings> meterReadings = new ArrayList<>();
        for (int i = 0; i < includedMonths.length; i++) {
        	meterReadings.add(new MeterReadings(connection, profile, shortMonths[includedMonths[i]].toUpperCase(), meter_readings[i]));
        }

        return meterReadings;
    }


}
