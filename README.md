# Meter Readings

This is RESTful web service that would process data from consumption meters, store valid data and provide consumptions for a given period of time.

Service is made in Spring MVC.

It uses embedded H2 database, which can be viewed at http://localhost:8080/h2

    Driver Class: org.h2.Driver
    JDBC URL: jdbc:h2:mem:typeqastdb
    User Name: root
    Password:


It is a Gradle project application so Gradle should be installed: https://gradle.org/install/

Run the application with:

    ./gradlew bootRun
    
Run the tests with:
    
    ./gradlew clean test
    
Test report can be found in /build/reports/tests/test/index.html

## Application behaviour and exception Handling

After successfully adding or updating data, list of names for valid and invalid profiles (or connections in MeterReadings part) will be shown.
For invalid profiles (or connection), next to a name is a reason why it was refused.

If there is no valid profiles (connections) to be saved, then custom error will be shown. Also shown with a list of profile names and reasons why they are refused.
Specific exceptions and handlers are made for every validation error that could happen, but in the end I decided to use only two - common and reusable ones:
EntityNotFoundException - with HTTP status code `404 Not Found`
InvalidEntitiesException - with HTTP status code `406 Not Acceptable`


## HTTP requests available:

Profile Ratio:

| HTTP Verb        | URL           | Description  | Status Codes |
| ----- |-----------|:-------------| ------|
| `GET` | `http://localhost:8080/ratios/all` | Obtains a list of all profile ratios. | `200 OK` if data found. `204 No Content` if no data found. |
| `GET` | `http://localhost:8080/ratios?profile={profile}` | Obtains all ratios for a profile stated in {profile} parameter. | `200 OK` if data found. `404 Not Found` if not found.|
| `GET` | `http://localhost:8080/ratios?month={month}` | Obtains all profiles ratios for a month stated in {month} parameter. | `200 OK` if data found. `404 Not Found` if not found.|
| `GET` | `http://localhost:8080/ratios?profile={profile}&month={month}` | Obtains all ratios for a profile and month stated in {profile} and {month} parameters. | `200 OK` if data found. `404 Not Found` if not found.|
| `POST` | `http://localhost:8080/ratios` | Creates new profile ratios based on the JSON contained in the request body. | `201 Created` If successfully created. `406 Not Acceptable` if not created.|
| `PUT` | `http://localhost:8080/ratios` | Updates profile ratios data with the JSON data contained in the request body. | `200 OK` if successfully updated. `406 Not Acceptable` if not updated. |
| `DELETE` | `http://localhost:8080/ratios/all` | Deletes all existing profile ratios. | `410 Gone` if successfully deleted. `204 No Content` if no data found. |
| `DELETE` | `http://localhost:8080/ratios?profile={profile}` | Deletes all ratios for a profile stated in {profile} parameter. | `410 Gone` if successfully deleted. `404 Not Found` if not found.|

Meter Readings:

| HTTP Verb        | URL           | Description  | Status Codes |
| ----- |-----------|:-------------| ------|
| `GET` | `http://localhost:8080/meters/all` | Obtains a list of all meter readings. | `200 OK` if data found. `204 No Content` if no data found. |
| `GET` | `http://localhost:8080/meters?profile={profile}` | Obtains all readings for a profile stated in {profile} parameter. | `200 OK` if data found. `404 Not Found` if not found.|
| `GET` | `http://localhost:8080/meters?connection={connection}` | Obtains all readings for a connection stated in {connection} parameter. | `200 OK` if data found. `404 Not Found` if not found.|
| `GET` | `http://localhost:8080/meters?connection={connection}&month={month}` | Obtains all readings for a connection and month stated in {connection} and {month} parameters. | `200 OK` if data found. `404 Not Found` if not found.|
| `POST` | `http://localhost:8080/meters` | Creates new meter readings based on the JSON contained in the request body. | `201 Created` If successfully created. `406 Not Acceptable` if not created.|
| `DELETE` | `http://localhost:8080/meters/all` | Deletes all existing meter readings. | `410 Gone` if successfully deleted. `204 No Content` if no data found. |
| `DELETE` | `http://localhost:8080/meters?profile={profile}` | Deletes all readings for a profile stated in {profile} parameter. | `410 Gone` if successfully deleted. `404 Not Found` if not found.|
| `DELETE` | `http://localhost:8080/meters?connection={connection}` | Deletes all readings for a connection stated in {connection} parameter. | `410 Gone` if successfully deleted. `404 Not Found` if not found.|